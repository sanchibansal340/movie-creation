import java.util.*;

public class MovieCreator {
    private String title;
    private String studio;
    private String rating;

    static int pgMoviesCount = 0;

    MovieCreator(String title, String studio, String rating) {
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    MovieCreator(String title, String studio) {
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }

    private static MovieCreator[] getPg(MovieCreator[] movies) {
        MovieCreator[] pgMovies = new MovieCreator[movies.length];

        for (MovieCreator movie : movies) {
            if (movie.rating.equals("PG")) {
                pgMovies[pgMoviesCount] = movie;
                pgMoviesCount++;
            }
        }
        return pgMovies;
    }

    private void display() {
        System.out.println(this.title + " produced by " + this.studio);
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of movies: ");
        int n = Integer.parseInt(scanner.nextLine());
        MovieCreator[] movies = new MovieCreator[n];

        for(int i = 0; i < n; i++) {
            System.out.println("Enter the title and studio of the movie " + (i+1));
            String title = scanner.nextLine();
            String studio = scanner.nextLine();

            System.out.println("Do you want to enter the movie's ratings (yes/no)");
            String choice = scanner.nextLine();

            if(choice.equals("yes")) {
                String ratings = scanner.nextLine();
                movies[i] = new MovieCreator(title, studio, ratings);
            }
            else {
                movies[i] = new MovieCreator(title, studio);
            }
        }

        scanner.close();

        MovieCreator[] pgMovies = MovieCreator.getPg(movies);

        System.out.println("PG Movies are: ");
        for (int i = 0; i < pgMoviesCount; i++) {
            pgMovies[i].display();
        }
    }
}
